import scrapy
import requests
from datetime import datetime, date, time, timedelta

# Otros imports
############################
import json
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose
from scrapy.spiders import Spider
from w3lib.html import remove_tags
#from bobot.items import BobotItem2
#################################

#Funcion para el automatizador de toma de registros diarios
# El scheduler usa esta funcion, crea un nombre y te baja el boletin en formato pdf
# No devuelve nada los sabados/domingos
def boletin_diario_caba():
    formato = "20%y%m%d"
    hoy = datetime.today()  # Asigna fecha-hora

    # Aplica formato
    fecha_hoy = hoy.strftime(formato)

    url = 'https://documentosboletinoficial.buenosaires.gob.ar/publico/{0}.pdf'.format(fecha_hoy)
    response = requests.get(url)

    with open('./boletines/bo_caba.pdf', 'wb') as f:
        f.write(response.content)


# Funcion que te baja por lote de fechas los boletines oficiales
# TODO: FALTA CONVERTIR A PROCESADOR POR LOTE DE FECHAS
class spider_caba(scrapy.Spider):
    name = "caba"

    def start_requests(self):
        #HAY QUE CREAR LA VARIABLE fechas_elegidas
        url = 'https://documentosboletinoficial.buenosaires.gob.ar/publico/{0}.pdf'.format(fechas_elegidas)
        yield scrapy.Request(url=url, callback=self.find_items)

    def find_items(self,response):#arreglar desde aqui
        with open('./bo_caba.pdf', 'wb') as f:
            f.write(response.body)
