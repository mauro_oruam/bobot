import json
from datetime import datetime, date, time, timedelta
import calendar
import schedule
import time
from scrap_provincias import scrap

with open('../data.json') as f:
    data_provincias = json.load(f)

todos_los_dias = []
martes_y_jueves = []
lu_mie_vie = []
martes_y_viernes = []
viernes = []
lunes_y_jueves = []

def task(lista):
    for prov in lista:
        scrap(prov)

for data in data_provincias:

    if data['dias_que_sale'] == 'Todos los días hábiles':
        todos_los_dias.append(data['provincia'])

    elif data['dias_que_sale'] == 'Martes y Jueves':
        martes_y_jueves.append(data['provincia'])

    elif data['dias_que_sale'] == 'Lunes, Miércoles y Viernes':
        lu_mie_vie.append(data['provincia'])

    elif data['dias_que_sale'] == 'Martes y Viernes':
        martes_y_viernes.append(data['provincia'])

    elif data['dias_que_sale'] == 'Viernes':
        viernes.append(data['provincia'])

    elif data['dias_que_sale'] == 'Lunes y Jueves':
        lunes_y_jueves.append(data['provincia'])


def schedule_actions():

    # Every Monday task() is called at 20:00
    schedule.every().monday.at('00:00').do(task(todos_los_dias))

    # Every Tuesday task() is called at 20:00
    schedule.every().tuesday.at('00:00').do(task(todos_los_dias))
    schedule.every().tuesday.at('00:00').do(task(martes_y_viernes))
    schedule.every().tuesday.at('00:00').do(task(martes_y_jueves))

    # Every Wednesday task() is called at 20:00
    schedule.every().wednesday.at('00:00').do(task(todos_los_dias))

    # Every Thursday task() is called at 20:00
    schedule.every().thursday.at('00:00').do(task(todos_los_dias))
    schedule.every().tuesday.at('00:00').do(task(martes_y_jueves))

    # Every Friday task() is called at 20:00
    schedule.every().friday.at('00:00').do(task(todos_los_dias))
    schedule.every().tuesday.at('00:00').do(task(martes_y_viernes))
    schedule.every().tuesday.at('00:00').do(task(viernes))

# Checks whether a scheduled task is pending to run or not
while True:
    schedule.run_pending()
    time.sleep(1)
