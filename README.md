# Bobot

Es una web app para que todos los dias hábiles por la mañana, tome todos los boletines oficiales del día de todas las provincias que lo hayan publicado y los procese con OCR.


## Servicios que ofrece BOBOT

Se puede filtrar por provincia para traer un boletin en especifico y crear una interfaz donde filtrar por palabras.
Con los boletines podemos ofrecer alertas personalizadas: seteo de keywords.
Mas adelante agregar la funcionalidad de buscar por rango de fechas (traer un lote de boletines).


## Requerimientos

Scrapy
```bash
pip install scrapy
```
Scheduler
```bash
pip install scheduler
```
Hacemos pruebas/test con Jupyter notebook

## Para desarrollar los boletines del dia:

```bash
python ./spiders/spider_ciudad_autonoma_de_buenos_aires.py
```
Aparecerá el pdf del último boletín publicado en /boletines
Los boletines publicados se encuentran: https://documentosboletinoficial.buenosaires.gob.ar/publico/20191206.pdf
Siendo su fecha el nombre del archivo
Empezamos con el boletin de CABA

## Para desarrollar la toma de boletines por rango de fechas:
```bash
scrapy crawl caba
```
Siendo caba el name dentro de la clase en spider_ciudad_autonoma_de_buenos_aires.py


## Contribuciones

Revisar los [Issues](https://gitlab.com/mauro_oruam/bobot/issues) creados en gitlab.com
Hacer updates de los mismos con el nombre del commit
